
require 'spec_helper'

require 'event_shipper'
require 'event_shipper/filters'

describe EventShipper::Proxy, 'with sample configuration' do
  let(:proxy) { described_class.new nil }
  before(:each) {
    proxy.configure_with(
      'listen' => {
        'host' => '127.0.0.1', 
        'port' => 5050 }, 
      'redis' => {
        'host' => '127.0.0.1', 
        'port' => '4567' }, 
      'encrypt' => true, 
      'users' => [
        'test/pass/word' ])
  }

  describe "#configure_with" do
    it "parses complex passwords correctly" do
      proxy.userdb.should == { 'test' => 'pass/word' }
    end
  end

  describe 'round trip test' do
    def send_message
      t = EventShipper::UDP.new('localhost', '5050')
      t.wrap EventShipper::Filter::Encrypt.new('test', 'pass/word')

      t.send(a: 1)
    end

    it "receives message and deciphers it" do
      flexmock(proxy).
        should_receive(:handle_message).with(
          {"a"=>1}, {:encrypting_user=>"test", :queue=>"queue"}).once.
        and_raise('quit!')

      Thread.start do
        sleep 0.01
        send_message
      end.abort_on_exception = true

      expect {
        proxy.main_loop
      }.to raise_error('quit!')

      proxy.transport.close
    end
  end
end