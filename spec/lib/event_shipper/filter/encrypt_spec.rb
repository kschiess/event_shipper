require 'spec_helper'

require 'event_shipper'
require 'event_shipper/filters'
require 'event_shipper/protocol'

describe EventShipper::Filter::AES256 do
  let(:encryption) { described_class.new('password') }
  describe "roundtrip" do
    let(:msg) { 'This is a secret message' }
    let(:enc) { encryption.enc(msg) }
    let(:dec) { encryption.dec(*enc) }

    describe 'encrypted message' do
      it 'should be longer than the original message' do
        enc.last.size.should > msg.size
      end
    end    
    describe 'decrypted message' do
      subject { dec }
      it { should == msg }
    end
  end  

end

describe "Filter::{Encrypt, Decrypt}" do
  include EventShipper::Protocol

  let(:encrypt) { EventShipper::Filter::Encrypt.new('test', 'password') }
  let(:decrypt) { EventShipper::Filter::Decrypt.new { |user| 'password'} }
  let(:attrs) { Hash.new }

  it 'encrypt and decrypt messages' do
    message = transmission(
      event(queue: 'queue', json: 'json')).serialize_to_string
  
    string = encrypt.en(message)    
    string.should_not include('queue')
    string.should include('test')
    string.size.should == 68

    attributes = {test: 'foo'}
    decrypted, a = decrypt.de(string, attrs)
    a.should == attrs

    attrs[:encrypting_user].should == 'test'

    reconstituted_message = parse_transmission(decrypted)
    reconstituted_message.version.should == 1
    reconstituted_message.events.size.should == 1

    event = reconstituted_message.events.first
    event.queue.should == 'queue'
  end
end