require 'spec_helper'

require 'event_shipper'

describe EventShipper::Filter::Transmission do
  let(:transmission) { described_class.new }
  
  let(:event) { EventShipper::Protocol.event({
    queue: 'queue',
    json: { key: 'value' }.to_json }) }
  let(:message) { transmission.en(event) }
  let(:attrs) { Hash.new }

  describe "#de" do
    it "returns hash and context" do
      hash, a = transmission.de(message, attrs)
      
      a.should == attrs

      attrs[:queue].should == 'queue'
      hash['key'].should == 'value'
    end
  end
end