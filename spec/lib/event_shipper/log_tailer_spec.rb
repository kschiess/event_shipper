require 'spec_helper'

require 'tempfile'
require 'event_shipper/log_tailer'

describe EventShipper::LogTailer do
  attr_reader :path
  before(:each) { 
    @tempfile = Tempfile.new('logtailer')
    @path = @tempfile.path
    @tempfile.close(false) }
  after(:each) { 
    @tempfile.close(true) }

  let(:transport) { flexmock('transport') }
  let!(:tailer) { described_class.new(path, transport) }

  let!(:producer_thread) {
    Thread.start do
      loop do
        File.open(path, 'w+') do |f|
          f.sync = true
          f.puts 'a line'
        end
        sleep 0.01
      end
    end
  }
  after(:each) { producer_thread.raise 'ABORT' }

  it "forwards event hashes to the transport" do
    transport.should_receive(:send).with(Hash).once
    tailer.thread_main true
  end
end