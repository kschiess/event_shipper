
require 'yajl/json_gem'

require 'event_shipper/protocol'

module EventShipper::Filter
  # Serializes a hash by wrapping it into a Protocol::Transmission object 
  # and then serializing it to a string. 
  #
  class Transmission
    include EventShipper::Protocol

    def en event
      transmission(event).serialize_to_string
    end
    def de string, attributes={}
      transmission = parse_transmission(string)

      if transmission.version != 1
        warn "Received a transmission with version #{transmission.version}, but this code parses version 1."
      end

      event = transmission.events.first
      attributes[:queue] = event.queue

      return JSON.parse(event.json), attributes

    rescue ProtocolBuffers::DecodeError
      return nil
    end
  end
end