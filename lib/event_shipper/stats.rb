
module EventShipper
  class Stats
    def initialize
      reset
    end

    def count_message
      @messages += 1
    end
    def count_failure
      @failures += 1
    end

    def reset
      @period_start = Time.now
      @messages = @failures = 0
    end

    def to_s
      seconds = Time.now - @period_start 

      mps = @messages / Float(seconds)
      fps = @failures / Float(seconds)

      "#{mps} messages/s, #{fps} failures/s"
    end
  end
end