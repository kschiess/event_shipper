
require 'time'
require 'socket'
require 'file-tail'

require 'event_shipper/event'

module EventShipper
  class LogTailer
    def initialize path, transport
      @path = path
      @transport = transport
      @host = Socket.gethostname
    end

    def start
      @thread = Thread.new(&method(:thread_main))
      @thread.abort_on_exception = true
    end

    def thread_main once=false
      File.open(@path) do |log|
        log.seek 0, IO::SEEK_END
        log.extend File::Tail

        log.max_interval = 5
        log.interval = 1

        log.tail do |line|
          issue line

          return if once
        end
      end
    end
    def issue line
      event = Event.new @host, @path, line
      @transport.send event.to_hash
    end

    def join
      @thread.join
    end
  end
end