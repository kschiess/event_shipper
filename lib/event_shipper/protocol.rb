
require_relative 'protocol/v1.pb.rb'

module EventShipper::Protocol

module_function
  def transmission *events
    Transmission.new(
      version: 1, 
      events: events)
  end

  def event attributes={}
    Event.new(attributes)
  end
  def encrypted attributes={}
    Encrypted.new(attributes)
  end

  def parse_encrypted string
    Encrypted.new.tap { |o|
      o.parse(string) }
  end
  def parse_transmission string
    Transmission.new.tap { |o| 
      o.parse(string) }
  end
end