
require 'addressable/uri'

module EventShipper
  class Event
    def initialize host, path, line
      @path = path
      @line = line
      @host = host
      @timestamp = Time.now.utc.iso8601(6)
      @source = Addressable::URI.new(
        :scheme => 'file', 
        :host => host, :path => path)
      @message = line.chomp.strip
      @fields = {}
      @tags = %w(shipped)
    end
    
    def to_hash
      {
        '@source' => @source.to_s,
        '@type' => @source.scheme,
        '@tags' => @tags,
        '@fields' => @fields,
        '@timestamp' => @timestamp,
        '@source_host' => @host,
        '@source_path' => @path,
        '@message' => @message
      }
    end
  end
end