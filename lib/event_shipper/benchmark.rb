
require 'benchmark'
require 'clamp'

module EventShipper
  class Benchmark < Clamp::Command
    def initialize *args
      super

      @s = UDP.new('localhost', 5050)
      @s.wrap Filter::Encrypt.new('user', 'password')

      @r = UDP.new('localhost', 5050)
      @r.wrap Filter::Decrypt.new { 'password' }
    end

    def execute
      puts "Simulates 1000 times the sending of a simple message: "
      measure do
        1000.times do _send_message end
      end

      puts "Simulates 1000 times the receiving of a message: "
      msg = _send_message
      measure do
        1000.times do _receive_message(msg) end
      end
    end

    def measure
      puts ::Benchmark.measure {
        yield
      }
    end

    def _send_message
      @s.encode(
        source: 'benchmark', 
        event: 'this is the event')
    end

    def _receive_message msg
      @r.decode(msg)
    end
  end
end