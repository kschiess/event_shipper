
require 'yaml'
require 'clamp'

require_relative 'udp'
require_relative 'log_tailer'

module EventShipper

  class Shipper < Clamp::Command
    option %w(-c --config), "FILE", "Location of the configuration file."

    def execute
      fail "Please specify a configuration file to use on the command line. (--config)" \
        unless config

      configuration = YAML.load_file(config)

      host, port = configuration['target'].split(':')
      transport = UDP.new(host, port)

      if configuration['encrypt'] 
        user, password = configuration.values_at('user', 'password')
        transport.wrap Filter::Encrypt.new(user, password)
      end

      tailers = configuration["logs"].map do |path|
        tailer = LogTailer.new(path, transport)
        tailer.start

        tailer
      end

      begin
        trap('SIGINFO') { puts transport.stats }
      rescue ArgumentError
        # unsupported signal SIGINFO
      end

      # Block until everyone exits.
      tailers.each do |tailer|
        tailer.join
      end
    end
  end
end