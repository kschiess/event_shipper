
require 'redis'
require 'yaml'
require 'clamp'

require_relative 'filters'
require_relative 'udp'

module EventShipper
  class Proxy < Clamp::Command

    option %w(-c --config), "FILE", "Location of the configuration file."
    option %w(-v --verbose), :flag, "Prints all events before forwarding them."

    def execute
      fail "Please specify a configuration file to use on the command line. (--config)" \
        unless config

      configure_with YAML.load_file(config)

      begin
        trap('SIGINFO') { puts transport.stats }
      rescue ArgumentError
        # unsupported signal SIGINFO
      end

      main_loop
    end

    def main_loop
      transport.dispatch { |queue, message| 
        handle_message queue, message }
    end
    def handle_message queue, message
      puts message if verbose?
      redis.lpush queue, message.to_json
    end

    attr_reader :host, :port
    attr_reader :listen
    attr_reader :userdb
    attr_reader :redis
    attr_reader :transport

    def configure_with configuration
      @listen = configuration['listen']
      @host, @port = listen['host'], listen['port']
      @transport = UDP.new(host, port)

      if configuration['encrypt']
        users = configuration['users']
        @userdb = Hash[users.map { |str| str.partition('/').values_at(0,2) }]

        transport.wrap Filter::Decrypt.new { |user| @userdb[user] }
      end

      redis_config = configuration['redis']
      @redis = Redis.new(
        host: redis_config['host'], 
        port: redis_config['port'])
    end
  end
end