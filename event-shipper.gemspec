# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = 'event-shipper'
  s.version = '0.2.4'

  s.authors = ['Kaspar Schiess']
  s.email = 'kaspar.schiess@absurd.li'
  s.extra_rdoc_files = ['README']
  s.files = %w(HISTORY LICENSE README) + Dir.glob("{lib,bin,configs}/**/*")
  s.executables = %w(esproxy esshipper)
  s.homepage = 'https://bitbucket.org/kschiess/event_shipper'
  s.rdoc_options = ['--main', 'README']
  s.require_paths = ['lib']
  s.summary = %Q(Ships log events to logstash.)
  s.description = %Q(
    event_shipper reads log files and sends each line in logstash JSON format 
    via encrypted UDP to redis.
  )

  %w(redis clamp file-tail addressable 
    yajl-ruby 
    ruby-protocol-buffers).each do |gem_name|
    s.add_dependency gem_name
  end  
  
  # %w(rspec flexmock rdoc sdoc guard guard-rspec rb-fsevent growl rake).
  #   each { |gem_name| 
  #     s.add_development_dependency gem_name }
end
